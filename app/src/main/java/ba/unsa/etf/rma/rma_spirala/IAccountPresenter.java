package ba.unsa.etf.rma.rma_spirala;

import android.os.Parcelable;

public interface IAccountPresenter {
    void create(double budget, double totalLimit, double monthLimit);
    void setAcc(Parcelable acc);
    Account getAcc();
}
