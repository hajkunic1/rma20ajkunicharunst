package ba.unsa.etf.rma.rma_spirala;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

public class TransactionAdapter extends ArrayAdapter<Transaction>{
    private int resource;

    public TransactionAdapter(@NonNull Context context, int resource, ArrayList<Transaction> transactions) {
        super(context, resource);
        this.resource = resource;
    }

    @SuppressLint("SetTextI18n")
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LinearLayout newView;
        if (convertView == null) {
            newView = new LinearLayout(getContext());
            String inflater = Context.LAYOUT_INFLATER_SERVICE;
            LayoutInflater li;
            li = (LayoutInflater)getContext().
                    getSystemService(inflater);
            li.inflate(resource, newView, true);
        } else {
            newView = (LinearLayout)convertView;
        }

        ColorDrawable white=new ColorDrawable();
        white.setColor(getContext().getColor(android.R.color.white));
        newView.setBackground(white);
        Transaction transaction = getItem(position);
        TextView title = newView.findViewById(R.id.title);
        TextView amount = newView.findViewById(R.id.t_amount);
        ImageView icon = newView.findViewById(R.id.icon);
        title.setText(transaction.getTitle());
        amount.setText(String.valueOf(transaction.getAmount()));
        if(transaction.getType().equals(TransactionType.INDIVIDUALINCOME)){
            icon.setImageResource(R.drawable.profit);
        }
        else if(transaction.getType().equals(TransactionType.REGULARINCOME)){
            icon.setImageResource(R.drawable.sales);
        }
        else if(transaction.getType().equals(TransactionType.PURCHASE)){
            icon.setImageResource(R.drawable.purchase);
        }
        else if(transaction.getType().equals(TransactionType.INDIVIDUALPAYMENT)){
            icon.setImageResource(R.drawable.pay);
        }
        else icon.setImageResource(R.drawable.pay1);
        return newView;
    }
    public void setTransactions(ArrayList<Transaction> transactions) {
        this.clear();
        this.addAll(transactions);
    }

    public Transaction getTransaction(int position) {return this.getItem(position);}

}
