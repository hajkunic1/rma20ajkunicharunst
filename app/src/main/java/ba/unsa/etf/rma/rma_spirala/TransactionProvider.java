package ba.unsa.etf.rma.rma_spirala;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.text.TextUtils;
import java.util.HashMap;
public class TransactionProvider extends ContentProvider {
    static final String PROVIDER_NAME = "ba.unsa.etf.rma.rma_spirala.cp";
    static final String URL = "content://" + PROVIDER_NAME + "/transactions";
    static final Uri CONTENT_URI = Uri.parse(URL);
    static final String _ID = "id";
    static final int TRANSACTIONS = 1;
    static final int TRANSACTION_ID = 2;
    static final UriMatcher uriMatcher;
    private static HashMap<String, String> TRANSACTION_PROJECTION_MAP;

    static {
        uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
        uriMatcher.addURI(PROVIDER_NAME, "transactions", TRANSACTIONS);
        uriMatcher.addURI(PROVIDER_NAME, "transactions/#", TRANSACTION_ID);
    }

    private SQLiteDatabase db;

    @Override public boolean onCreate() {
        Context context = getContext();
        DBOpenHelper dbHelper = new DBOpenHelper(context);
        db = dbHelper.getWritableDatabase();
        return (db == null)? false:true;
    }
    @Override
    public Uri insert(Uri uri, ContentValues values) {
        long rowID = db.insert(DBOpenHelper.TRANSACTION_TABLE, "", values);
        if (rowID < 0) {
            throw new SQLException("Failed to insert into" + uri);
        }
        else {
            Uri _uri = ContentUris.withAppendedId(CONTENT_URI, rowID);
            getContext().getContentResolver().notifyChange(_uri, null);
            return _uri;

        }
    }
    @Override
    public Cursor query(Uri uri, String[] projection, String selection,
                        String[] selectionArgs, String sortOrder) {
        SQLiteQueryBuilder qb = new SQLiteQueryBuilder();
        qb.setTables(DBOpenHelper.TRANSACTION_TABLE);
        switch (uriMatcher.match(uri)) {
            case TRANSACTIONS:
                qb.setProjectionMap(TRANSACTION_PROJECTION_MAP);
                break;
            case TRANSACTION_ID:
                qb.appendWhere(_ID + "=" + uri.getPathSegments().get(1));
                break;
            default:
                throw new IllegalArgumentException("Unknown URI " + uri);
        }

        Cursor c = qb.query(db, projection, selection, selectionArgs,
                null, null, sortOrder);
        c.setNotificationUri(getContext().getContentResolver(), uri);
        return c;
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        int count = 0;
        switch (uriMatcher.match(uri)) {
            case TRANSACTIONS:
                count = db.delete(DBOpenHelper.TRANSACTION_TABLE, selection, selectionArgs);
                break;
            case TRANSACTION_ID:
                String id = uri.getPathSegments().get(1);
                count = db.delete(DBOpenHelper.TRANSACTION_TABLE, _ID + " = " + id +
                        (!TextUtils.isEmpty(selection) ? " AND (" +
                                selection + ')' : ""), selectionArgs);
                break;
            default:
                throw new IllegalArgumentException("Unknown URI " + uri);
        }
        getContext().getContentResolver().notifyChange(uri, null);
        return count;
    }
    @Override
    public int update(Uri uri, ContentValues values, String selection,
                      String[] selectionArgs) {
        int count = 0;
        switch (uriMatcher.match(uri)) {
            case TRANSACTIONS:
                count = db.update(DBOpenHelper.TRANSACTION_TABLE, values, selection,
                        selectionArgs);
                break;
            case TRANSACTION_ID:
                count = db.update(DBOpenHelper.TRANSACTION_TABLE, values, _ID + " = " +
                        uri.getPathSegments().get(1) +
                        (!TextUtils.isEmpty(selection) ? " AND (" +
                                selection + ')' : ""), selectionArgs);
                break;
            default:
                throw new IllegalArgumentException("Unknown URI " + uri );
        }
        getContext().getContentResolver().notifyChange(uri, null);
        return count;
    }
    @Override
    public String getType(Uri uri) {
        switch (uriMatcher.match(uri)) {
            case TRANSACTIONS:
                return "vnd.android.cursor.dir/vnd.example.students";
            case TRANSACTION_ID:
                return "vnd.android.cursor.item/vnd.example.students";
            default:
                throw new IllegalArgumentException("Unsupported URI: " + uri);
        }
    }
    public void deleteRows(){
        db.execSQL("DELETE FROM "+ DBOpenHelper.TRANSACTION_TABLE);
    }
}