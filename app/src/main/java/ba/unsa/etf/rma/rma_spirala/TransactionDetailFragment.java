package ba.unsa.etf.rma.rma_spirala;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.regex.Pattern;

public class TransactionDetailFragment extends Fragment {
    public static boolean undo=false;
    private EditText title,startDate,endDate,description,amount,interval;
    private Spinner spinnerType;
    private Button deleteBtn, saveBtn;
    private boolean validateOK=true;
    private Context context;
    private LayerDrawable bottomBorder= getBorders(Color.LTGRAY,R.color.colorPrimaryDark,0,0,0,10);
    boolean validateT=true,validateEDate=true,validateSDate=true,validateDes=true,validateA=true,validateI=true,isItUpdate=false;
    private DataTransfer transfer;
    private Change onChange;
    private Transaction tmpTransaction;
    private static int count=0;
    private TextView mode;

    private TransactionDetailPresenter transactionPresenter;
    public TransactionDetailPresenter getPresenter() {
        if (transactionPresenter == null) {
            transactionPresenter = new TransactionDetailPresenter(getActivity());
        }
        return transactionPresenter;
    }
    public interface DataTransfer{
        public void passData(Transaction t, Transaction t1, String action);
    }
    public interface Change{
        public void onChange(String sortBy,String filterBy,String date);
    }

    @Override
    public View onCreateView(
            LayoutInflater inflater,
            final ViewGroup container,
            Bundle savedInstanceState) {
        View fragmentView = inflater.inflate(R.layout.fragment_detail, container, false);
        context=getActivity();
        title=fragmentView.findViewById(R.id.title_input);
        startDate=fragmentView.findViewById(R.id.start_date_input);
        endDate=fragmentView.findViewById(R.id.end_date_input);
        description=fragmentView.findViewById(R.id.description_input);
        amount=fragmentView.findViewById(R.id.amount_input);
        interval=fragmentView.findViewById(R.id.interval_input);
        spinnerType=fragmentView.findViewById(R.id.spinnerType);
        deleteBtn=fragmentView.findViewById(R.id.delete_btn);
        saveBtn=fragmentView.findViewById(R.id.save_btn);
        transfer=(DataTransfer) getActivity();
        title.setBackground(bottomBorder);
        startDate.setBackground(bottomBorder);
        endDate.setBackground(bottomBorder);
        description.setBackground(bottomBorder);
        amount.setBackground(bottomBorder);
        interval.setBackground(bottomBorder);
        onChange=(Change) getActivity();
        mode=fragmentView.findViewById(R.id.dmode_id);
        ArrayList<String> types=new ArrayList<String>(){
            {
                add("Type");
                add("INDIVIDUALPAYMENT");
                add("REGULARPAYMENT");
                add("PURCHASE");
                add("INDIVIDUALINCOME");
                add("REGULARINCOME");
            }
        };
        final FilterBySpinnerAdapter spinnerTypeAdapter=new FilterBySpinnerAdapter(getActivity(),R.layout.filter_by_spinner_element,types);
        spinnerType.setAdapter(spinnerTypeAdapter);
        spinnerType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                description.setEnabled(true);
                interval.setEnabled(true);
                endDate.setEnabled(true);
                if(description.getBackground() instanceof ColorDrawable){
                    if(((ColorDrawable) description.getBackground()).getColor()==getResources().getColor(android.R.color.background_light)){
                        description.setBackground(bottomBorder);
                    }
                }
                if(spinnerType.getSelectedItemPosition()!=0){
                    interval.setBackground(bottomBorder);
                    endDate.setBackground(bottomBorder);
                    if(spinnerType.getSelectedItemPosition()==4||spinnerType.getSelectedItemPosition()==5){
                        if(description.getText().length()>0){
                            description.setText("");
                        }
                        description.setEnabled(false);
                        description.setBackgroundColor(getResources().getColor(android.R.color.background_light));
                    }
                    if(spinnerType.getSelectedItemPosition()!=2&&spinnerType.getSelectedItemPosition()!=5){

                        if(interval.getText().length()>=0){
                            interval.setText("");
                            interval.setBackgroundColor(getResources().getColor(android.R.color.background_light));
                        }else interval.setBackgroundColor(getResources().getColor(R.color.validate_green));
                        if(endDate.getText().length()>=0){
                            endDate.setText("");
                            endDate.setBackgroundColor(getResources().getColor(android.R.color.background_light));
                        }else endDate.setBackgroundColor(getResources().getColor(R.color.validate_green));
                        interval.setEnabled(false);
                        endDate.setEnabled(false);
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        if (getArguments() != null && getArguments().containsKey("transaction")) {
            if(getArguments().getParcelable("transaction")!=null){
                if(ConnectivityBroadcastReceiver.isNetworkAvailable){
                    mode.setText("");
                }else{
                    mode.setText(R.string.offline_change);
                }
                isItUpdate=true;
                getPresenter().setTransaction(getArguments().getParcelable("transaction"));
                if(!ConnectivityBroadcastReceiver.isNetworkAvailable&&getPresenter().getTransaction().getTitle().length()>7&&getPresenter().getTransaction().getTitle().substring(0,7).equals("deleted")){
                    deleteBtn.setText("UNDO");
                }
                tmpTransaction=getPresenter().getTransaction();
                getPresenter().getTransaction().setId(((Transaction)getArguments().getParcelable("transaction")).getId());
                title.setText(getPresenter().getTransaction().getTitle());
                startDate.setText(getPresenter().getTransaction().getDate().format(DateTimeFormatter.ofPattern("dd.MM.yyyy")));
                if(getPresenter().getTransaction().getEndDate()!=null)
                    endDate.setText(getPresenter().getTransaction().getEndDate().format(DateTimeFormatter.ofPattern("dd.MM.yyyy")));
                else endDate.setText("");
                if(getPresenter().getTransaction().getItemDescription()!=null)
                    description.setText(getPresenter().getTransaction().getItemDescription());
                else description.setText("");
                amount.setText(String.valueOf(getPresenter().getTransaction().getAmount()));
                if(getPresenter().getTransaction().getTransactionInterval()!=null)
                    interval.setText(String.valueOf(getPresenter().getTransaction().getTransactionInterval()));
                else interval.setText("");
                String type=getPresenter().getTransaction().getType().name();
                int tmp=0;
                if(type.equals("INDIVIDUALPAYMENT")){
                    tmp=1;
                }
                else if(type.equals("REGULARPAYMENT")){
                    tmp=2;
                }
                else if(type.equals("PURCHASE")){
                    tmp=3;
                }
                else if(type.equals("INDIVIDUALINCOME")){
                    tmp=4;
                }
                else if (type.equals("REGULARINCOME")) tmp=5;

                spinnerType.setSelection(tmp);
            }else{
                if(ConnectivityBroadcastReceiver.isNetworkAvailable){
                    mode.setText("");
                }else{
                    mode.setText(R.string.offline_add);
                }
                deleteBtn.setEnabled(false);
                title.setBackground(bottomBorder);
                startDate.setBackground(bottomBorder);
                endDate.setBackground(bottomBorder);
                description.setBackground(bottomBorder);
                amount.setBackground(bottomBorder);
                interval.setBackground(bottomBorder);
                validateA=false;
                validateDes=false;
                validateEDate=false;
                validateI=false;
                validateSDate=false;
                validateT=false;
            }

        }
        deleteBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(ConnectivityBroadcastReceiver.isNetworkAvailable||getPresenter().getTransaction().getInternal_id()==0){
                        deleteBtn.setText("DELETE");
                        new AlertDialog.Builder(context)
                                .setTitle("Delete transaction")
                                .setMessage("Are you sure you want to delete this transaction?")
                                .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        transfer.passData(getPresenter().getTransaction(),null,"delete");
                                        onChange.onChange(getArguments().getString("sortBy"),getArguments().getString("filterBy"),getArguments().getString("date"));
                                    }
                                })
                                .setNegativeButton(R.string.no, null)
                                .setIcon(R.drawable.alert)
                                .show();
                    }else{
                        count++;
                        if(count==1){
                            getPresenter().getTransaction().setTitle("deleted ("+getPresenter().getTransaction().getTitle()+")");
                            transfer.passData(getPresenter().getTransaction(),null,"add");
                        }
                        if(count==2){
                            transfer.passData(getPresenter().getTransaction(),null,"delete");
                            count=0;
                        }

                    }


                }
            });
        saveBtn.setOnClickListener(new View.OnClickListener() {
            boolean validateLimit=true;
            @Override
            public void onClick(View v) {
                LocalDate dateTmp;
                double amountTmp;
                String titleTmp;
                String itemDescriptionTmp;
                Integer transactionIntervalTmp;
                LocalDate endDateTmp;
                TransactionType typeTmp;

                if(!validateT||!validateSDate||!validateA||spinnerType.getSelectedItemPosition()==0){
                    validateOK=false;
                }else if(spinnerType.getSelectedItemPosition()>=1&&spinnerType.getSelectedItemPosition()<=3&&!validateDes) {
                    validateOK = false;
                    description.setBackgroundColor(getResources().getColor(R.color.validate_red));
                }else if((spinnerType.getSelectedItemPosition()==2||spinnerType.getSelectedItemPosition()==5)&&(!validateEDate||!validateI)){
                    if(!validateEDate){
                        validateOK=false;
                        endDate.setBackgroundColor(getResources().getColor(R.color.validate_red));
                    }
                    if(!validateI){
                        validateOK=false;
                        interval.setBackgroundColor(getResources().getColor(R.color.validate_red));
                    }
                }else validateOK=true;
                if(validateOK) {
                    dateTmp = LocalDate.parse(startDate.getText(), DateTimeFormatter.ofPattern("dd.MM.yyyy"));
                    amountTmp = Double.parseDouble(amount.getText().toString());
                    titleTmp = title.getText().toString();

                    if (description.getText().length() == 0) {
                        itemDescriptionTmp = null;
                    } else itemDescriptionTmp = description.getText().toString();
                    if (interval.getText().length() == 0) {
                        transactionIntervalTmp = null;
                    } else transactionIntervalTmp = Integer.parseInt(interval.getText().toString());
                    if (endDate.getText().length() == 0) {
                        endDateTmp = null;
                    } else
                        endDateTmp = LocalDate.parse(endDate.getText(), DateTimeFormatter.ofPattern("dd.MM.yyyy"));
                    typeTmp = TransactionType.valueOf(spinnerTypeAdapter.getType(spinnerType.getSelectedItemPosition()));
                    getPresenter().create(dateTmp,amountTmp,typeTmp,titleTmp,itemDescriptionTmp,transactionIntervalTmp,endDateTmp,(Account) getArguments().getParcelable("acc"));

                    if(getPresenter().getTransaction().getAccount().getMonthLimit()<Double.parseDouble(String.valueOf(amount.getText()))||
                            getPresenter().getTransaction().getAccount().getTotalLimit()<Double.parseDouble(String.valueOf(amount.getText()))){
                        validateLimit=false;
                    }
                }
                if(validateOK&&validateLimit){
                    if(isItUpdate){
                            transfer.passData(getPresenter().getTransaction(),tmpTransaction,"update");
                    }else transfer.passData(getPresenter().getTransaction(),null,"add");
                      onChange.onChange(getArguments().getString("sortBy"),getArguments().getString("filterBy"),getArguments().getString("date"));
                }else if(!validateOK){
                    new AlertDialog.Builder(context)
                            .setTitle("Wrong input")
                            .setMessage("Please input valid informations!")
                            .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            })
                            .show();
                }else if (!validateLimit){
                    new AlertDialog.Builder(context)
                            .setTitle("Warning")
                            .setMessage("The amount of transaction in greater than account limits.Do you want to proceed aniway?")
                            .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    if(isItUpdate) {
                                        transfer.passData(getPresenter().getTransaction(), tmpTransaction, "update");
                                    }else transfer.passData(getPresenter().getTransaction(), null, "add");
                                    onChange.onChange(getArguments().getString("sortBy"),getArguments().getString("filterBy"),getArguments().getString("date"));
                                }
                            })
                            .setNegativeButton(R.string.no,null)
                            .setIcon(R.drawable.alert)
                            .show();

                }
            }
        });

        title.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                title.setBackgroundColor(getResources().getColor(R.color.validate_green));
                if(title.getText().length()<=3||title.getText().length()>=15){
                    validateT=false;
                    title.setBackgroundColor(getResources().getColor(R.color.validate_red));
                }else validateT=true;

            }
        });
        startDate.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                startDate.setBackgroundColor(getResources().getColor(R.color.validate_green));
                String dateRegEx="^\\d{1,2}\\.\\d{1,2}\\.\\d{4}$";
                if(!Pattern.matches(dateRegEx,s)){
                    validateSDate=false;
                    startDate.setBackgroundColor(getResources().getColor(R.color.validate_red));
                }else validateSDate=true;

            }
        });
        endDate.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                endDate.setBackgroundColor(getResources().getColor(R.color.validate_green));
                String dateRegEx="^\\d{1,2}\\.\\d{1,2}\\.\\d{4}$";
                if(!Pattern.matches(dateRegEx,s)){
                    validateEDate=false;
                    endDate.setBackgroundColor(getResources().getColor(R.color.validate_red));
                }else validateEDate=true;
            }
        });
        description.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                description.setBackgroundColor(getResources().getColor(R.color.validate_green));
                if(description.getText().length()==0){
                    validateDes=false;
                    description.setBackgroundColor(getResources().getColor(R.color.validate_red));
                }else validateDes=true;

            }
        });
        amount.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                amount.setBackgroundColor(getResources().getColor(R.color.validate_green));
                if(amount.getText().length()==0){
                    validateA=false;
                    amount.setBackgroundColor(getResources().getColor(R.color.validate_red));
                }else validateA=true;

            }
        });
        interval.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                interval.setBackgroundColor(getResources().getColor(R.color.validate_green));
                if(interval.getText().length()==0){
                    validateI=false;
                    interval.setBackgroundColor(getResources().getColor(R.color.validate_red));
                }else validateI=true;
            }
        });

    return fragmentView;

    }
    private LayerDrawable getBorders(int bgColor, int borderColor,
                                       int left, int top, int right, int bottom){
        ColorDrawable borderColorDrawable = new ColorDrawable(borderColor);
        ColorDrawable backgroundColorDrawable = new ColorDrawable(bgColor);
        Drawable[] drawables = new Drawable[]{
                borderColorDrawable,
                backgroundColorDrawable
        };

        LayerDrawable layerDrawable = new LayerDrawable(drawables);

        layerDrawable.setLayerInset(
                1,
                left,
                top,
                right,
                bottom
        );
        return layerDrawable;
    }

}
