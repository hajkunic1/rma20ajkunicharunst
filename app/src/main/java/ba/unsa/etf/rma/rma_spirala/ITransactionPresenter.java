package ba.unsa.etf.rma.rma_spirala;

public interface ITransactionPresenter {
    void refreshTransactions();
}
