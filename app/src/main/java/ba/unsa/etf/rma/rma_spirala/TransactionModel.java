package ba.unsa.etf.rma.rma_spirala;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;

class TransactionModel {
    private static Account acc=AccountModel.account;
    public static ArrayList<Transaction> transactions = new ArrayList<Transaction>(){
        {
            add(new Transaction(LocalDate.of(2020,3,2),25,TransactionType.INDIVIDUALPAYMENT,"Transaction2","Lunch",null,null,acc));
            add(new Transaction(LocalDate.of(2020,4,3),30,TransactionType.PURCHASE,"Transaction3","T-Shirt",null,null,acc));
            add(new Transaction(LocalDate.of(2020,1,4),45,TransactionType.REGULARINCOME,"Transaction4",null,10,LocalDate.of(2020,12,21),acc));
            add(new Transaction(LocalDate.of(2020,5,5),22,TransactionType.REGULARPAYMENT,"Transaction5","Scholarship",10,LocalDate.of(2020,10,21),acc));
            add(new Transaction(LocalDate.of(2020,7,7),66,TransactionType.INDIVIDUALPAYMENT,"Transaction7","Taxes",null,null,acc));
            add(new Transaction(LocalDate.of(2020,2,6),33,TransactionType.INDIVIDUALINCOME,"Transaction6",null,null,null,acc));
            add(new Transaction(LocalDate.of(2020,6,8),77,TransactionType.PURCHASE,"Transaction8","Bike",null,null,acc));
            add(new Transaction(LocalDate.of(2020,3,9),12,TransactionType.REGULARINCOME,"Transaction9",null,20,LocalDate.of(2020,9,21),acc));
            add(new Transaction(LocalDate.of(2020,5,10),21,TransactionType.REGULARPAYMENT,"Transaction10","Car repair",21,LocalDate.of(2020,11,21),acc));
            add(new Transaction(LocalDate.of(2020,2,3),10,TransactionType.INDIVIDUALINCOME,"Transaction1",null,null,null,acc));
            add(new Transaction(LocalDate.of(2020,8,11),32,TransactionType.INDIVIDUALINCOME,"Transaction11",null,null,null,acc));
            add(new Transaction(LocalDate.of(2020,12,12),44,TransactionType.INDIVIDUALPAYMENT,"Transaction12","Haircut",null,null,acc));
            add(new Transaction(LocalDate.of(2020,10,13),98,TransactionType.PURCHASE,"Transaction13","Shoes",null,null,acc));
            add(new Transaction(LocalDate.of(2020,9,14),78,TransactionType.REGULARINCOME,"Transaction14",null,14,LocalDate.of(2020,12,21),acc));
            add(new Transaction(LocalDate.of(2020,3,15),67,TransactionType.REGULARPAYMENT,"Transaction15","Membership",12,LocalDate.of(2020,10,21),acc));
            add(new Transaction(LocalDate.of(2020,6,16),7,TransactionType.INDIVIDUALINCOME,"Transaction16",null,null ,null,acc));
            add(new Transaction(LocalDate.of(2020,7,17),5,TransactionType.INDIVIDUALPAYMENT,"Transaction17","Repair",null,null,acc));
            add(new Transaction(LocalDate.of(2020,3,18),11,TransactionType.PURCHASE,"Transaction18","Blanket",null,null,acc));
            add(new Transaction(LocalDate.of(2020,7,19),92,TransactionType.REGULARINCOME,"Transaction19",null,6,LocalDate.of(2020,8,21),acc));
            add(new Transaction(LocalDate.of(2020,2,6),25,TransactionType.REGULARPAYMENT,"Transaction20","Phone bill",8,LocalDate.of(2020,5,21),acc));
        }

    };

    public static void delete(Transaction transaction){
        transactions.remove(transaction);
    }
    public static void addTransaction(Transaction transaction){
        transaction.setAccount(transactions.get(0).getAccount());
        transactions.add(transaction);
        double tmp=transactions.get(0).getAccount().getBudget();
        if(transaction.getType().getType()>=1&&transaction.getType().getType()<=3){
            transactions.get(0).getAccount().setBudget(tmp-transaction.getAmount());
        }else{
            transactions.get(0).getAccount().setBudget(tmp+transaction.getAmount());
        }

    }
    public static void updateTransaction(Transaction newTransaction,Transaction oldTransaction){
        Transaction transaction=oldTransaction;
        double tmp=transactions.get(0).getAccount().getBudget();
        if(newTransaction.getType().getType()>=1&&newTransaction.getType().getType()<=3&&oldTransaction.getType().getType()>3){
            transactions.get(0).getAccount().setBudget(tmp-newTransaction.getAmount());
        }else if(newTransaction.getType().getType()>3&&oldTransaction.getType().getType()<4){
            transactions.get(0).getAccount().setBudget(tmp+newTransaction.getAmount());
        }
        for (Transaction t :
                transactions) {
            if (t.equals(oldTransaction)) {
                t.setAmount(newTransaction.getAmount());
                t.setDate(newTransaction.getDate());
                t.setEndDate(newTransaction.getEndDate());
                t.setItemDescription(newTransaction.getItemDescription());
                t.setTitle(newTransaction.getTitle());
                t.setTransactionInterval(newTransaction.getTransactionInterval());
                t.setType(newTransaction.getType());
                break;
            }
        }
    }
    public static void setAcc(){
        acc=AccountModel.account;
        for (Transaction t :
                transactions) {
            t.setAccount(acc);
        }
    }
    public static ArrayList<Transaction> getAllMonthTransactions(){
        ArrayList<Transaction> result=new ArrayList<>(transactions);
        final ArrayList<Transaction>tmp=new ArrayList<>();
        Collections.sort(result, new Comparator<Transaction>() {
            @Override
            public int compare(Transaction o1, Transaction o2) {
                if(o1.getDate().getMonth().equals(o2.getDate().getMonth())){
                    if(o1.getAmount()>o2.getAmount()){
                        tmp.add(o2);
                    }else tmp.add(o1);
                }
                return o1.getDate().compareTo(o2.getDate());
            }
        });
        for (int i = 0; i < tmp.size(); i++) {
            result.remove(tmp.get(i));
        }
        return result;

    }
    public static ArrayList<Transaction> getAllWeekTransactions(){
        ArrayList<Transaction> result=new ArrayList<>();

        for (int i = 0; i < transactions.size();i++) {
            if(isDateInCurrentWeek(java.sql.Date.valueOf(transactions.get(i).getDate().toString()))){
                result.add(transactions.get(i));
            }
        }
        final ArrayList<Transaction>tmp=new ArrayList<>();
        Collections.sort(result, new Comparator<Transaction>() {
            @Override
            public int compare(Transaction o1, Transaction o2) {
                    if(o1.getAmount()>o2.getAmount()){
                        tmp.add(o2);
                    }else tmp.add(o1);
                return o1.getDate().compareTo(o2.getDate());
            }
        });
        for (int i = 0; i < tmp.size(); i++) {
            result.remove(tmp.get(i));
        }
        return result;


    }
    public static ArrayList<Transaction> getAllDayTransactions(){
        ArrayList<Transaction> result=new ArrayList<>();
        final ArrayList<Transaction>tmp=new ArrayList<>();

        for (int i = 0; i < transactions.size();i++) {
            if(transactions.get(i).getDate().getMonth().equals(LocalDate.now().getMonth())&&transactions.get(i).getDate().getDayOfMonth()==LocalDate.now().getDayOfMonth()&&transactions.get(i).getDate().getYear()==LocalDate.now().getYear()){
                result.add(transactions.get(i));
            }
        }
        Collections.sort(result, new Comparator<Transaction>() {
            @Override
            public int compare(Transaction o1, Transaction o2) {
                    if(o1.getAmount()>o2.getAmount()){
                        tmp.add(o2);
                    }else tmp.add(o1);

                return o1.getDate().compareTo(o2.getDate());
            }
        });
        for (int i = 0; i < tmp.size(); i++) {
            result.remove(tmp.get(i));
        }
        return result;

    }
    public static boolean isDateInCurrentWeek(Date date) {
        Calendar currentCalendar = Calendar.getInstance();
        int week = currentCalendar.get(Calendar.WEEK_OF_YEAR);
        int year = currentCalendar.get(Calendar.YEAR);
        Calendar targetCalendar = Calendar.getInstance();
        targetCalendar.setTime(date);
        int targetWeek = targetCalendar.get(Calendar.WEEK_OF_YEAR);
        int targetYear = targetCalendar.get(Calendar.YEAR);
        return week == targetWeek && year == targetYear;
    }
}
