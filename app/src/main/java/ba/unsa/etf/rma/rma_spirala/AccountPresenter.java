package ba.unsa.etf.rma.rma_spirala;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Parcelable;

public class AccountPresenter implements IAccountPresenter,AccountInteractor.OnAccGetDone{
    private Account account;
    private AccountInteractor interactor;
    private Context context;

    public AccountPresenter(Context context){
        this.interactor=new AccountInteractor((AccountInteractor.OnAccGetDone)this);
        this.context=context;
    }

    public void updateAccount(Account account){
        if(ConnectivityBroadcastReceiver.isNetworkAvailable){
            new AccountInteractor((AccountInteractor.OnAccGetDone)this).execute("update",String.valueOf(account.getTotalLimit()),String.valueOf(account.getMonthLimit()),String.valueOf(account.getBudget()));
        }else{
            String url=AcconutProvider.CONTENT_URI+"/1";
            Uri uri=Uri.parse(url);
            ContentValues values=new ContentValues();
            values.put(DBOpenHelper.MONTH_LIMIT,account.getMonthLimit());
            values.put(DBOpenHelper.TOTAL_LIMIT,account.getTotalLimit());
            values.put(DBOpenHelper.BUDGET,account.getBudget());
            values.put(DBOpenHelper.ACTION,"update");
            context.getContentResolver().update(uri,values,null,null);
        }

    }

    @Override
    public void create(double budget, double totalLimit, double monthlLimit) {
        account=new Account(budget,totalLimit,monthlLimit);
    }

    @Override
    public void setAcc(Parcelable acc) {
        this.account=(Account)acc;
    }

    @Override
    public Account getAcc() {
        return account;
    }
    public void get(){
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if(cm.getActiveNetworkInfo()!=null){
            String url=AcconutProvider.CONTENT_URI+"/1";
            Uri uri=Uri.parse(url);
            Cursor cursor=context.getContentResolver().query(uri,null,null,null);
            if(cursor!=null&&cursor.moveToFirst()){
                int idPos = cursor.getColumnIndexOrThrow(DBOpenHelper.ACCOUNT_ID);
                int budgetPos = cursor.getColumnIndexOrThrow(DBOpenHelper.BUDGET);
                int mLimitPos = cursor.getColumnIndexOrThrow(DBOpenHelper.MONTH_LIMIT);
                int tLimitPos = cursor.getColumnIndexOrThrow(DBOpenHelper.TOTAL_LIMIT);
                int actPos = cursor.getColumnIndexOrThrow(DBOpenHelper.ACTION);
                Account acc= new Account(Double.parseDouble(cursor.getString(budgetPos)),Double.parseDouble(cursor.getString(tLimitPos)),Double.parseDouble(cursor.getString(mLimitPos)));
                acc.setId(cursor.getInt(idPos));;
                if(cursor.getString(actPos).equals("update")){
                    updateAccount(acc);
                }
            }
            new AccountInteractor((AccountInteractor.OnAccGetDone)this).execute("get");
        }else{
            String url=AcconutProvider.CONTENT_URI+"/1";
            Uri uri=Uri.parse(url);
            Cursor cursor=context.getContentResolver().query(uri,null,null,null);
            if(cursor!=null&&cursor.moveToFirst()){
                int idPos = cursor.getColumnIndexOrThrow(DBOpenHelper.ACCOUNT_ID);
                int budgetPos = cursor.getColumnIndexOrThrow(DBOpenHelper.BUDGET);
                int mLimitPos = cursor.getColumnIndexOrThrow(DBOpenHelper.MONTH_LIMIT);
                int tLimitPos = cursor.getColumnIndexOrThrow(DBOpenHelper.TOTAL_LIMIT);
                Account acc= new Account(Double.parseDouble(cursor.getString(budgetPos)),Double.parseDouble(cursor.getString(tLimitPos)),Double.parseDouble(cursor.getString(mLimitPos)));
                acc.setId(cursor.getInt(idPos));;
                this.account=acc;
            }
        }
    }

    @Override
    public void onDone(Account acc) {
            this.account=acc;
    }

}
