package ba.unsa.etf.rma.rma_spirala;

import java.util.ArrayList;

public interface ITransactionView {

    void setTransactions(ArrayList<Transaction>transactions);
    void notifyTransactionListDataSetChanged();

}
