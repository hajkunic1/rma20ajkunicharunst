package ba.unsa.etf.rma.rma_spirala;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

public class AccountFragment extends Fragment{
    private AccountPresenter accountPresenter;
    private EditText amount,totalLimit,monthLimit;
    private TextView mode;
    private Button update;
    private OnSwipe onSwipe;
    private LayerDrawable bottomBorder= getBorders(Color.LTGRAY,R.color.colorPrimaryDark,0,0,0,10);


    public AccountPresenter getPresenter() {
        if (accountPresenter == null) {
            accountPresenter = new AccountPresenter(getActivity());
        }
        return accountPresenter;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(
            LayoutInflater inflater,
            ViewGroup container,
            Bundle savedInstanceState) {
        View fragmentView = inflater.inflate(R.layout.account_fragment, container, false);
        amount=fragmentView.findViewById(R.id.global_amount_input);
        totalLimit=fragmentView.findViewById(R.id.total_limit_input);
        monthLimit=fragmentView.findViewById(R.id.month_limit_input);
        amount.setBackground(bottomBorder);
        totalLimit.setBackground(bottomBorder);
        monthLimit.setBackground(bottomBorder);
        mode=fragmentView.findViewById(R.id.mode_id);
        if(ConnectivityBroadcastReceiver.isNetworkAvailable){
            mode.setText("");
        }else{
            mode.setText(R.string.offline_change);
        }
        update=fragmentView.findViewById(R.id.updateAcc);
        onSwipe=(OnSwipe) getActivity();
        if(getArguments()!=null&&getArguments().containsKey("acc")){
            getPresenter().setAcc(getArguments().getParcelable("acc"));
            amount.setText(String.valueOf(getPresenter().getAcc().getBudget()));
            totalLimit.setText(String.valueOf(getPresenter().getAcc().getTotalLimit()));
            monthLimit.setText(String.valueOf(getPresenter().getAcc().getMonthLimit()));
        }

        update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getPresenter().create(Double.parseDouble(amount.getText().toString()),Double.parseDouble(totalLimit.getText().toString()),Double.parseDouble(monthLimit.getText().toString()));
                getPresenter().updateAccount(getPresenter().getAcc());
                onSwipe.onSwipe("right","account");
            }
        });
    fragmentView.setOnTouchListener(new OnSwipeTouchListener(getActivity()){
        @Override
            public void onSwipeRight(){
                onSwipe.onSwipe("right","account");
            }
        @Override
            public void onSwipeLeft(){
            onSwipe.onSwipe("left","account");
        }
    });

    return fragmentView;
    }
    private LayerDrawable getBorders(int bgColor, int borderColor,
                                     int left, int top, int right, int bottom) {
        ColorDrawable borderColorDrawable = new ColorDrawable(borderColor);
        ColorDrawable backgroundColorDrawable = new ColorDrawable(bgColor);
        Drawable[] drawables = new Drawable[]{
                borderColorDrawable,
                backgroundColorDrawable
        };

        LayerDrawable layerDrawable = new LayerDrawable(drawables);

        layerDrawable.setLayerInset(
                1,
                left,
                top,
                right,
                bottom
        );
        return layerDrawable;
    }

}
