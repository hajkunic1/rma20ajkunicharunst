package ba.unsa.etf.rma.rma_spirala;

import android.graphics.Movie;
import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;

public class AccountInteractor  extends AsyncTask<String, Integer, Void> implements IAccountInteractor {
private Account account;
private OnAccGetDone caller;

    @Override
    public Account getAccount(){
        return account;
    }
    public AccountInteractor (OnAccGetDone accGetDone){
        caller=accGetDone;
        account=new Account();
    }

    @Override
    protected Void doInBackground(String... strings) {
        String type=null,total=null,global=null,month=null;
        try {
            type = URLEncoder.encode(strings[0], "utf-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        if(type.equals("update")){
            try {
                total= URLEncoder.encode(strings[1], "utf-8");
                month = URLEncoder.encode(strings[2], "utf-8");
                global = URLEncoder.encode(strings[3], "utf-8");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            String url1 = "http://rma20-app-rmaws.apps.us-west-1.starter.openshift-online.com/account/a7d3a420-de54-4760-8fe3-77ca7247fb1e";
            try {
                URL url = new URL(url1);
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setRequestMethod("POST");
                conn.setRequestProperty("Content-Type", "application/json");
                conn.setRequestProperty("Accept","application/json");
                conn.setDoOutput(true);
                conn.setDoInput(true);
                conn.connect();


                JSONObject jsonParam = new JSONObject();
                jsonParam.put("budget", global);
                jsonParam.put("monthLimit", month);
                jsonParam.put("totalLimit", total);

                DataOutputStream os = new DataOutputStream(conn.getOutputStream());
                os.writeBytes(jsonParam.toString());
                os.flush();
                os.close();
                Log.i("STATUS", String.valueOf(conn.getResponseCode()));
                Log.i("MSG" , conn.getResponseMessage());
                conn.disconnect();
            } catch (Exception e) {

            }

        }else{
            String url1 = "http://rma20-app-rmaws.apps.us-west-1.starter.openshift-online.com/account/a7d3a420-de54-4760-8fe3-77ca7247fb1e";
            try {
                URL url = new URL(url1);
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                InputStream in = new BufferedInputStream(urlConnection.getInputStream());
                String result = convertStreamToString(in);
                JSONObject jo = new JSONObject(result);
                Double budget=jo.getDouble("budget");
                Double totalLimit=jo.getDouble("totalLimit");
                Double monthLimit=jo.getDouble("monthLimit");
                this.account=new Account(budget,totalLimit,monthLimit);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid){
        super.onPostExecute(aVoid);
        caller.onDone(account);
    }

    public interface OnAccGetDone{
        public void onDone(Account acc);
    }

    public String convertStreamToString(InputStream is) {
        BufferedReader reader = new BufferedReader(new
                InputStreamReader(is));
        StringBuilder sb = new StringBuilder();
        String line = null;
        try {
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
        } catch (IOException e) {
        } finally {
            try {
                is.close();
            } catch (IOException e) {
            }
        }
        return sb.toString();
    }


}
