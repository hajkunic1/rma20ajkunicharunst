package ba.unsa.etf.rma.rma_spirala;

import android.os.Parcelable;

import java.time.LocalDate;

public interface ITransactionDetailPresenter {
    void create(LocalDate date, double amount, TransactionType type, String title, String itemDescription, Integer transactionInterval, LocalDate endDate, Account account);
    Transaction getTransaction();
    void setTransaction(Parcelable transaction);
}
