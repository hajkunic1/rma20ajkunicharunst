package ba.unsa.etf.rma.rma_spirala;

public enum TransactionType {
    INDIVIDUALPAYMENT(5),
    REGULARPAYMENT(1),
    PURCHASE(3),
    INDIVIDUALINCOME(4),
    REGULARINCOME(2)
    ;
    private final int type;
    TransactionType(int type){
        this.type=type;
    }
    public int getType(){
        return type;
    }
}
