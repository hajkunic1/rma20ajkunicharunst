package ba.unsa.etf.rma.rma_spirala;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.ArrayList;
import java.util.List;

public class FilterBySpinnerAdapter extends ArrayAdapter<String> {
    private int resource;
    private ArrayList<String> types;

    public FilterBySpinnerAdapter(@NonNull Context context, int resource, @NonNull ArrayList<String> objects) {
        super(context, resource, objects);
        this.resource=resource;
        this.types=objects;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LinearLayout newView;
        if (convertView == null) {
            newView = new LinearLayout(getContext());
            String inflater = Context.LAYOUT_INFLATER_SERVICE;
            LayoutInflater li;
            li = (LayoutInflater)getContext().
                    getSystemService(inflater);
            li.inflate(resource, newView, true);
        } else {
            newView = (LinearLayout)convertView;
        }

        TextView name=newView.findViewById(R.id.spinner_name);
        ImageView icon=newView.findViewById(R.id.spinner_icon);
        name.setText(getItem(position));
        if(getItem(position).equals("Filter by")){
            icon.setImageResource(R.drawable.filter);
        }else if(getItem(position).equals("ALL")){
            icon.setImageResource(R.drawable.all);
        }else if(getItem(position).equals("INDIVIDUALINCOME")){
            icon.setImageResource(R.drawable.profit);
        }else if(getItem(position).equals("REGULARINCOME")){
            icon.setImageResource(R.drawable.sales);
        }else if(getItem(position).equals("PURCHASE")){
            icon.setImageResource(R.drawable.purchase);
        }else if(getItem(position).equals("INDIVIDUALPAYMENT")){
            icon.setImageResource(R.drawable.pay);
        }else if(getItem(position).equals("REGULARPAYMENT")){
            icon.setImageResource(R.drawable.pay1);
        }
        return newView;
    }

    public String getType(int position){
        return this.getItem(position);
    }
    @Override
    public View getDropDownView(int position, View convertView, @NonNull ViewGroup parent) {
        return getView(position, convertView, parent);
    }
}
