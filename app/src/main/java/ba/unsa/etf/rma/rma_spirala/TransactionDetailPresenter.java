package ba.unsa.etf.rma.rma_spirala;

import android.content.Context;
import android.os.Parcelable;

import java.time.LocalDate;

public class TransactionDetailPresenter implements ITransactionDetailPresenter {
    private Transaction transaction;
    private Context context;


    public TransactionDetailPresenter(Context context){this.context=context;}

    @Override
    public void create(LocalDate date, double amount, TransactionType type, String title, String itemDescription, Integer transactionInterval, LocalDate endDate, Account account) {
        transaction=new Transaction(date,amount,type,title,itemDescription,transactionInterval,endDate,account);
    }

    @Override
    public Transaction getTransaction() {
        return transaction;
    }

    @Override
    public void setTransaction(Parcelable transaction) {
        this.transaction=(Transaction)transaction;
    }


}

