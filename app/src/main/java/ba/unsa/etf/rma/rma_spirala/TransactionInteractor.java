package ba.unsa.etf.rma.rma_spirala;

import android.os.AsyncTask;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class TransactionInteractor extends AsyncTask<String, Integer, Void> implements ITransactionInteractor{
private ArrayList<Transaction> transactions;
private  OnTransactionsGetDone caller;
    @Override
    public ArrayList<Transaction> getTransactions() {
        return transactions;
    }
    public TransactionInteractor(OnTransactionsGetDone onTransactionsGetDone){
        caller=onTransactionsGetDone;
        transactions=new ArrayList<>();
    }
    @Override
    protected Void doInBackground(String... strings) {
        String type = null;
        try {
            type = URLEncoder.encode(strings[0], "utf-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
       if(type.equals("add")){
           String url1 = "http://rma20-app-rmaws.apps.us-west-1.starter.openshift-online.com/account/a7d3a420-de54-4760-8fe3-77ca7247fb1e/transactions/";
           String typeId="",transactionInterval="",itemDescription="",endDate="",amount="",title="",date="";
           try {
               date= URLEncoder.encode(strings[1], "utf-8");
               title = URLEncoder.encode(strings[2], "utf-8");
               amount = URLEncoder.encode(strings[3], "utf-8");
               endDate = URLEncoder.encode(strings[4], "utf-8");
               itemDescription = URLEncoder.encode(strings[5], "utf-8");
               transactionInterval = URLEncoder.encode(strings[6], "utf-8");
               typeId = URLEncoder.encode(strings[7], "utf-8");
           } catch (UnsupportedEncodingException e) {
               e.printStackTrace();
           }
           try {
               URL url = new URL(url1);
               HttpURLConnection conn = (HttpURLConnection) url.openConnection();
               conn.setRequestMethod("POST");
               conn.setRequestProperty("Content-Type", "application/json");
               conn.setRequestProperty("Accept","application/json");
               conn.setDoOutput(true);
               conn.setDoInput(true);
               conn.connect();

               JSONObject jsonParam = new JSONObject();
               jsonParam.put("date", date);
               jsonParam.put("title", title);
               jsonParam.put("endDate", endDate);
               jsonParam.put("itemDescription", itemDescription);
               jsonParam.put("transactionInterval", transactionInterval);
               jsonParam.put("amount", amount);
               jsonParam.put("typeId", typeId);
               DataOutputStream os = new DataOutputStream(conn.getOutputStream());
               os.writeBytes(jsonParam.toString());
               os.flush();
               os.close();
               Log.i("STATUS", String.valueOf(conn.getResponseCode()));
               Log.i("MSG" , conn.getResponseMessage());
               conn.disconnect();
           } catch (Exception e) {

           }
       }
        else if(type.equals("update")){
            String id = null;
            try {
                id=URLEncoder.encode(strings[8], "utf-8");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            String url1 = "http://rma20-app-rmaws.apps.us-west-1.starter.openshift-online.com/account/a7d3a420-de54-4760-8fe3-77ca7247fb1e/transactions/"+id;
            String typeId="",transactionInterval="",itemDescription="",endDate="",amount="",title="",date="";
            try {
                date= URLEncoder.encode(strings[1], "utf-8");
                title = URLEncoder.encode(strings[2], "utf-8");
                amount = URLEncoder.encode(strings[3], "utf-8");
                endDate = URLEncoder.encode(strings[4], "utf-8");
                itemDescription = URLEncoder.encode(strings[5], "utf-8");
                transactionInterval = URLEncoder.encode(strings[6], "utf-8");
                typeId = URLEncoder.encode(strings[7], "utf-8");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            try {
                URL url = new URL(url1);
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setRequestMethod("POST");
                conn.setRequestProperty("Content-Type", "application/json");
                conn.setRequestProperty("Accept","application/json");
                conn.setDoOutput(true);
                conn.setDoInput(true);
                conn.connect();

                JSONObject jsonParam = new JSONObject();
                jsonParam.put("date", date);
                jsonParam.put("title", title);
                jsonParam.put("endDate", endDate);
                jsonParam.put("itemDescription", itemDescription);
                jsonParam.put("transactionInterval", transactionInterval);
                jsonParam.put("amount", amount);
                jsonParam.put("typeId", typeId);

                DataOutputStream os = new DataOutputStream(conn.getOutputStream());
                os.writeBytes(jsonParam.toString());
                os.flush();
                os.close();
                Log.i("STATUS", String.valueOf(conn.getResponseCode()));
                Log.i("MSG" , conn.getResponseMessage());
                conn.disconnect();
            } catch (Exception e) {

            }
        }
        else if(type.equals("delete")){
            String id = null;
            try {
                id = URLEncoder.encode(strings[1], "utf-8");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            String url1 = "http://rma20-app-rmaws.apps.us-west-1.starter.openshift-online.com/account/a7d3a420-de54-4760-8fe3-77ca7247fb1e/transactions/"+id;

            URL url = null;
            try {
                url = new URL(url1);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
            try {
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                urlConnection.setRequestMethod("DELETE");
                InputStream in = new BufferedInputStream(urlConnection.getInputStream());
                String result = convertStreamToString(in);
                JSONObject jo = new JSONObject(result);
            } catch (IOException | JSONException e) {
                e.printStackTrace();
            }
        }
        else if(type.equals("all")){
            int page=0;
            while(true){
                String url1 = "http://rma20-app-rmaws.apps.us-west-1.starter.openshift-online.com/account/a7d3a420-de54-4760-8fe3-77ca7247fb1e/transactions?page="+page;
                page++;
                try {
                    URL url = new URL(url1);
                    HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                    InputStream in = new BufferedInputStream(urlConnection.getInputStream());
                    String result = convertStreamToString(in);
                    JSONObject jo = new JSONObject(result);
                    JSONArray results = jo.getJSONArray("transactions");
                    for (int i =0;i<results.length();i++){
                        JSONObject transaction =results.getJSONObject(i);
                        LocalDate date= LocalDate.parse(transaction.getString("date").substring(0,10));
                        Double amount = transaction.getDouble("amount");
                        String title;
                        if(transaction.getString("title").equals("null")){
                            title = null;
                        }else title = transaction.getString("title");
                        String itemDescription;
                        if(transaction.getString("itemDescription").equals("null")){
                            itemDescription=null;
                        }else itemDescription=transaction.getString("itemDescription");
                        Integer transactionInterval;
                        if(transaction.getString("transactionInterval").equals("null")){
                            transactionInterval=null;
                        }else  transactionInterval = transaction.getInt("transactionInterval");
                        LocalDate endDate;
                        if(transaction.getString("endDate").equals("null")){
                            endDate=null;
                        }else endDate = LocalDate.parse(transaction.getString("endDate").substring(0,10));
                        transactions.add(new Transaction(date, amount, TransactionType.INDIVIDUALPAYMENT, title, itemDescription, transactionInterval, endDate,null));
                        transactions.get(i).setInternal_id(transaction.getInt("id"));
                        if(transaction.getInt("TransactionTypeId")==1){
                            transactions.get(i).setType(TransactionType.REGULARPAYMENT);
                        }else if(transaction.getInt("TransactionTypeId")==2){
                            transactions.get(i).setType(TransactionType.REGULARINCOME);
                        }else if(transaction.getInt("TransactionTypeId")==3){
                            transactions.get(i).setType(TransactionType.PURCHASE);
                        }else if(transaction.getInt("TransactionTypeId")==4){
                            transactions.get(i).setType(TransactionType.INDIVIDUALINCOME);
                        }else transactions.get(i).setType(TransactionType.INDIVIDUALPAYMENT);
                    }
                    if(results.length()<5){
                        break;
                    }
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }else {
            transactions.clear();
            String sortBy = null,filterBy=null,month=null,year=null;
            try {
                sortBy = URLEncoder.encode(strings[1], "utf-8");
                filterBy = URLEncoder.encode(strings[2], "utf-8");
                month = URLEncoder.encode(strings[3], "utf-8");
                year = URLEncoder.encode(strings[4], "utf-8");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            int page=0;
            while(true){
                String url1="";
                if(filterBy.equals("all")&&sortBy.equals("all")){
                    url1 = "http://rma20-app-rmaws.apps.us-west-1.starter.openshift-online.com/account/a7d3a42" +
                            "0-de54-4760-8fe3-77ca7247fb1e/transactions/filter?year="+year+"&month="+month+"&page="+page;

                }else if(filterBy.equals("all")&&!sortBy.equals("all")){
                    url1 = "http://rma20-app-rmaws.apps.us-west-1.starter.openshift-online.com/account/a7d3a42" +
                            "0-de54-4760-8fe3-77ca7247fb1e/transactions/filter?sort="+sortBy+"&year="+year+"&month="+month+"&page="+page;
                }else if(!filterBy.equals("all")&&sortBy.equals("all")){
                    url1 = "http://rma20-app-rmaws.apps.us-west-1.starter.openshift-online.com/account/a7d3a42" +
                            "0-de54-4760-8fe3-77ca7247fb1e/transactions/filter?typeId="+filterBy+"&year="+year+"&month="+month+"&page="+page;
                }else{
                    url1 = "http://rma20-app-rmaws.apps.us-west-1.starter.openshift-online.com/account/a7d3a42" +
                            "0-de54-4760-8fe3-77ca7247fb1e/transactions/filter?typeId="+filterBy+"&year="+year+"&month="+month+"&page="+page+"&sort="+sortBy;
                }
                page++;
                try {
                    URL url = new URL(url1);
                    HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                    InputStream in = new BufferedInputStream(urlConnection.getInputStream());
                    String result = convertStreamToString(in);
                    JSONObject jo = new JSONObject(result);
                    JSONArray results = jo.getJSONArray("transactions");
                    if(results.length()==0) break;
                    for (int i =0;i<results.length();i++){
                        JSONObject transaction =results.getJSONObject(i);
                        LocalDate date= LocalDate.parse(transaction.getString("date").substring(0,10));
                        Double amount = transaction.getDouble("amount");
                        String title;
                        if(transaction.getString("title").equals("null")){
                            title = null;
                        }else title = transaction.getString("title");
                        String itemDescription=transaction.getString("itemDescription");
                        Integer transactionInterval;
                        if(transaction.getString("transactionInterval").equals("null")){
                            transactionInterval=null;
                        }else  transactionInterval = transaction.getInt("transactionInterval");
                        LocalDate endDate;
                        if(transaction.getString("endDate").equals("null")){
                            endDate=null;
                        }else endDate = LocalDate.parse(transaction.getString("endDate").substring(0,10));
                        transactions.add(new Transaction(date, amount, TransactionType.INDIVIDUALPAYMENT, title, itemDescription, transactionInterval, endDate,null));
                        transactions.get(i).setInternal_id(transaction.getInt("id"));
                        if(transaction.getInt("TransactionTypeId")==1){
                            transactions.get(i).setType(TransactionType.REGULARPAYMENT);
                        }else if(transaction.getInt("TransactionTypeId")==2){
                            transactions.get(i).setType(TransactionType.REGULARINCOME);
                        }else if(transaction.getInt("TransactionTypeId")==3){
                            transactions.get(i).setType(TransactionType.PURCHASE);
                        }else if(transaction.getInt("TransactionTypeId")==4){
                            transactions.get(i).setType(TransactionType.INDIVIDUALINCOME);
                        }else transactions.get(i).setType(TransactionType.INDIVIDUALPAYMENT);
                        //transactions.get(i).setId(transaction.getInt("id"));

                    }
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

        }

        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid){
        super.onPostExecute(aVoid);
        caller.onDone(transactions);
    }

    public interface OnTransactionsGetDone{
        public void onDone(ArrayList<Transaction> transactions);
    }

    public String convertStreamToString(InputStream is) {
        BufferedReader reader = new BufferedReader(new
                InputStreamReader(is));
        StringBuilder sb = new StringBuilder();
        String line = null;
        try {
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
        } catch (IOException e) {
        } finally {
            try {
                is.close();
            } catch (IOException e) {
            }
        }
        return sb.toString();
    }





    public void deleteTransaction(int id) {
        String url1 = "http://rma20-app-rmaws.apps.us-west-1.starter.openshift-online.com/account/a7d3a420-de54-4760-8fe3-77ca7247fb1e/transactions/"+id;

        URL url = null;
        try {
            url = new URL(url1);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        try {
            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setRequestMethod("DELETE");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void addTransaction(Transaction transaction) {
        transactions.add(transaction);
    }
    public void updateTransaction(Transaction newTransaction, Transaction oldTransaction) {
        TransactionModel.updateTransaction(newTransaction, oldTransaction);
    }
    public ArrayList<Transaction> getAllDayTransactions(){
        return TransactionModel.getAllDayTransactions();
    }
    public ArrayList<Transaction> getAllWeekTransactions(){
        return TransactionModel.getAllWeekTransactions();
    }
    public ArrayList<Transaction> getAllMonthTransactions(){
        return TransactionModel.getAllMonthTransactions();
    }
}
