package ba.unsa.etf.rma.rma_spirala;

import android.os.Parcelable;

public interface IAccountInteractor {
    Account getAccount();
}
