package ba.unsa.etf.rma.rma_spirala;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.text.TextUtils;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.HashMap;

public class AcconutProvider extends ContentProvider {
    static final String PROVIDER_NAME = "ba.unsa.etf.rma.rma_spirala.acp";
    static final String URL = "content://" + PROVIDER_NAME + "/account";
    static final Uri CONTENT_URI = Uri.parse(URL);
    static final String _ID = "id";
    static final int ACC = 1;
    static final int ACC_ID = 2;
    static final UriMatcher uriMatcher;
    private static HashMap<String, String> TRANSACTION_PROJECTION_MAP;

    static {
        uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
        uriMatcher.addURI(PROVIDER_NAME, "account", ACC);
        uriMatcher.addURI(PROVIDER_NAME, "account/#", ACC_ID);
    }

    private SQLiteDatabase db;

    @Override public boolean onCreate() {
        Context context = getContext();
        DBOpenHelper dbHelper = new DBOpenHelper(context);
        db = dbHelper.getWritableDatabase();
        return (db == null)? false:true;
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection,
                        String[] selectionArgs, String sortOrder) {
        SQLiteQueryBuilder qb = new SQLiteQueryBuilder();
        qb.setTables(DBOpenHelper.ACCOUNT_TABLE);
        qb.appendWhere(_ID + "=" + uri.getPathSegments().get(1));
        Cursor c = qb.query(db, projection, selection, selectionArgs,
                null, null, sortOrder);
        c.setNotificationUri(getContext().getContentResolver(), uri);
        return c;
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        return 0;
    }
    @Override
    public int update(Uri uri, ContentValues values, String selection,
                      String[] selectionArgs) {
        int count = 0;
        switch (uriMatcher.match(uri)) {
            case ACC:
                count = db.update(DBOpenHelper.ACCOUNT_TABLE, values, selection,
                        selectionArgs);
                break;
            case ACC_ID:
                count = db.update(DBOpenHelper.ACCOUNT_TABLE, values, _ID + " = " +
                        uri.getPathSegments().get(1) +
                        (!TextUtils.isEmpty(selection) ? " AND (" +
                                selection + ')' : ""), selectionArgs);
                break;
            default:
                throw new IllegalArgumentException("Unknown URI " + uri );
        }
        getContext().getContentResolver().notifyChange(uri, null);
        return count;
    }
    @Override
    public String getType(Uri uri) {
        switch (uriMatcher.match(uri)) {
            case ACC:
                return "vnd.android.cursor.dir/vnd.example.students";
            case ACC_ID:
                return "vnd.android.cursor.item/vnd.example.students";
            default:
                throw new IllegalArgumentException("Unsupported URI: " + uri);
        }
    }

    @Override
    public Uri insert(Uri uri, ContentValues values) {
        long rowID = db.insert(DBOpenHelper.ACCOUNT_TABLE, "", values);
        if (rowID < 0) {
            throw new SQLException("Failed to insert into" + uri);
        }
        else {
            Uri _uri = ContentUris.withAppendedId(CONTENT_URI, rowID);
            getContext().getContentResolver().notifyChange(_uri, null);
            return _uri;

        }
    }


}

