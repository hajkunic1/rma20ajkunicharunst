package ba.unsa.etf.rma.rma_spirala;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.os.Parcelable;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import androidx.fragment.app.Fragment;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.Description;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.formatter.IndexAxisValueFormatter;
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet;
import com.github.mikephil.charting.utils.ColorTemplate;

import java.util.ArrayList;

public class GraphsFragment extends Fragment {
    private OnSwipe onSwipe;
    private ArrayList<BarEntry> barEntryIncome,barEntryPayment,barEntryAll;
    private ArrayList<String> monthsIncome,monthsPayment,monthsAll;
    private ArrayList<Transaction>transactions;
    private Spinner spinner;
    private AccountInteractor account;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    } @SuppressLint("ClickableViewAccessibility")
    @Override
    public View onCreateView(
            LayoutInflater inflater,
            ViewGroup container,
            Bundle savedInstanceState) {
        View fragmentView = inflater.inflate(R.layout.graphs_layout, container, false);
        final BarChart chart = fragmentView.findViewById(R.id.income);
        barEntryIncome = new ArrayList<BarEntry>();
        monthsPayment=new ArrayList<>();
        monthsIncome=new ArrayList<>();
        monthsAll=new ArrayList<>();
        barEntryAll=new ArrayList<BarEntry>();
        barEntryPayment=new ArrayList<BarEntry>();
        barEntryIncome=new ArrayList<BarEntry>();
        spinner=fragmentView.findViewById(R.id.graphs_spinner);
        ArrayAdapter<CharSequence> adapter=ArrayAdapter.createFromResource(getContext(),R.array.graph_spinner_items,android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        spinner.setSelection(2);
        onSwipe=(OnSwipe)getActivity();
        if(getArguments()!=null&&getArguments().containsKey("month")){
            setTransactions(getArguments().<Transaction>getParcelableArrayList("month"));
        }
        BarDataSet barDataSet=new BarDataSet(barEntryIncome,"Amount");
        barDataSet.setColors(ColorTemplate.JOYFUL_COLORS);
        Description description=new Description();
        description.setText("Months");
        chart.setDescription(description);
        BarData barData=new BarData(barDataSet);
        chart.setData(barData);
        XAxis x=chart.getXAxis();
        x.setValueFormatter(new IndexAxisValueFormatter(monthsIncome));
        x.setPosition(XAxis.XAxisPosition.TOP);
        x.setLabelRotationAngle(270);
        x.setLabelCount(monthsIncome.size());

        final BarChart chart1 = fragmentView.findViewById(R.id.spent);

        BarDataSet barDataSet1=new BarDataSet(barEntryPayment,"Amount");
        barDataSet1.setColors(ColorTemplate.PASTEL_COLORS);
        chart1.setDescription(description);
        BarData barData1=new BarData(barDataSet1);
        chart1.setData(barData1);
        XAxis x1=chart1.getXAxis();
        x1.setValueFormatter(new IndexAxisValueFormatter(monthsPayment));
        x1.setPosition(XAxis.XAxisPosition.TOP);
        x1.setLabelRotationAngle(270);
        x1.setLabelCount(monthsPayment.size());


        final BarChart chart2 = fragmentView.findViewById(R.id.all);

        BarDataSet barDataSet2=new BarDataSet(barEntryAll,"Amount");
        barDataSet2.setColors(ColorTemplate.MATERIAL_COLORS);
        chart2.setDescription(description);
        BarData barData2=new BarData(barDataSet2);
        chart2.setData(barData2);
        XAxis x2=chart2.getXAxis();
        x2.setValueFormatter(new IndexAxisValueFormatter(monthsAll));
        x2.setPosition(XAxis.XAxisPosition.TOP);
        x2.setLabelRotationAngle(270);
        x2.setLabelCount(monthsAll.size());


        fragmentView.setOnTouchListener(new OnSwipeTouchListener(getContext()){

            @Override
                public void onSwipeRight(){
                onSwipe.onSwipe(getArguments().<Transaction>getParcelableArrayList("month").get(0).getAccount());
            }
            @Override
            public void onSwipeLeft(){
                onSwipe.onSwipe("left","graphs");
            }

        });
        chart.setOnTouchListener(new OnSwipeTouchListener(getActivity()){
            @Override
            public void onSwipeRight(){
                onSwipe.onSwipe(getArguments().<Transaction>getParcelableArrayList("month").get(0).getAccount());
            }
            @Override
            public void onSwipeLeft(){
                onSwipe.onSwipe("left","graphs");
            }
        });
        chart1.setOnTouchListener(new OnSwipeTouchListener(getActivity()){
            @Override
            public void onSwipeRight(){
                onSwipe.onSwipe(getArguments().<Transaction>getParcelableArrayList("month").get(0).getAccount());
            }
            @Override
            public void onSwipeLeft(){
                onSwipe.onSwipe("left","graphs");
            }
        });
        chart2.setOnTouchListener(new OnSwipeTouchListener(getActivity()){
            @Override
            public void onSwipeRight(){
                onSwipe.onSwipe(getArguments().<Transaction>getParcelableArrayList("month").get(0).getAccount());
            }
            @Override
            public void onSwipeLeft(){
                onSwipe.onSwipe("left","graphs");
            }
        });



    return fragmentView;
    }

    private void setTransactions(ArrayList<Transaction> transactions){
        int counter1=0,counter2=0,counter3=0;
        for (int i = 0; i <transactions.size() ; i++) {
            if (transactions.get(i).getType().equals(TransactionType.INDIVIDUALINCOME) || transactions.get(i).getType().equals(TransactionType.REGULARINCOME)){
                barEntryIncome.add(new BarEntry(counter1, (float) transactions.get(i).getAmount()));
                monthsIncome.add(transactions.get(i).getDate().getMonth().toString());
                counter1++;
            }else if(transactions.get(i).getType().equals(TransactionType.PURCHASE) || transactions.get(i).getType().equals(TransactionType.REGULARPAYMENT)||transactions.get(i).getType().equals(TransactionType.INDIVIDUALPAYMENT)){
                barEntryPayment.add(new BarEntry(counter2, (float) transactions.get(i).getAmount()));
                monthsPayment.add(transactions.get(i).getDate().getMonth().toString());
                counter2++;
            }
            barEntryAll.add(new BarEntry(counter3, (float) transactions.get(i).getAmount()));
            monthsAll.add(transactions.get(i).getDate().getMonth().toString());
            counter3++;

        }
    }
}
