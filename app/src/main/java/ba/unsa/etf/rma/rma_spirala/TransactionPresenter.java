package ba.unsa.etf.rma.rma_spirala;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;

import java.lang.reflect.Array;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;

public class TransactionPresenter implements ITransactionPresenter,TransactionInteractor.OnTransactionsGetDone {
    private ITransactionView view;
    private TransactionInteractor interactor;
    private Context context;

    public TransactionPresenter(ITransactionView view, Context context){
        this.view=view;
        this.interactor=new TransactionInteractor((TransactionInteractor.OnTransactionsGetDone)this);
        this.context=context;
    }
    @Override
    public void refreshTransactions(){
        view.setTransactions(interactor.getTransactions());
        view.notifyTransactionListDataSetChanged();
    }
    public void listSort(String sortBy, String filterBy, LocalDate date) {
        ArrayList<Transaction> sorted = interactor.getTransactions();
        String filter="",sort="",month="",year="";
        if (sortBy.equals("Price - Ascending")) {
            sort="amount.asc";
        } else if (sortBy.equals("Price - Descending")) {
            sort="amount.desc";
        } else if (sortBy.equals("Title - Ascending")) {
            sort="title.asc";
        } else if (sortBy.equals("Title - Descending")) {
            sort="title.desc";
        } else if (sortBy.equals("Date - Ascending")) {
            sort="date.asc";
        } else if (sortBy.equals("Date - Descending")) {
            sort="date.desc";
        }else sort="all";

        if (filterBy.equals("ALL"))filter="all";
        else if (filterBy.equals("INDIVIDUALINCOME")) {
            filter="4";
        } else if (filterBy.equals("REGULARINCOME")) {
            filter="2";
        } else if (filterBy.equals("PURCHASE")) {
            filter="3";
        } else if (filterBy.equals("INDIVIDUALPAYMENT")) {
            filter="5";
        } else  filter="1";
        year=String.valueOf(date.getYear());
        if(date.getMonthValue()<10){
            month="0";
        }
        month+=String.valueOf(date.getMonthValue());
        if(ConnectivityBroadcastReceiver.isNetworkAvailable){
            new TransactionInteractor((TransactionInteractor.OnTransactionsGetDone)this).execute("sort",sort,filter,month,year);
        }

    }

    private ArrayList<Transaction> myFilter(TransactionType type, ArrayList<Transaction> transactions) {
        ArrayList<Transaction> result = new ArrayList<>();
        for (int i = 0; i < transactions.size(); i++) {
            if (type.equals(transactions.get(i).getType())) {
                result.add(transactions.get(i));
            }
        }
        return result;
    }

    private ArrayList<Transaction> listFilter(String filterBy, ArrayList<Transaction> transactions) {
        ArrayList<Transaction> filtered;
        if (filterBy.equals("Filter by")) return new ArrayList<>();
        if (filterBy.equals("ALL")) return transactions;
        if (filterBy.equals("INDIVIDUALINCOME")) {
            filtered = myFilter(TransactionType.INDIVIDUALINCOME, transactions);
        } else if (filterBy.equals("REGULARINCOME")) {
            filtered = myFilter(TransactionType.REGULARINCOME, transactions);
        } else if (filterBy.equals("PURCHASE")) {
            filtered = myFilter(TransactionType.PURCHASE, transactions);
        } else if (filterBy.equals("INDIVIDUALPAYMENT")) {
            filtered = myFilter(TransactionType.INDIVIDUALPAYMENT, transactions);
        } else filtered = myFilter(TransactionType.REGULARPAYMENT, transactions);
        return filtered;
    }

    public void filterByMonth(LocalDate date) {
        ArrayList<Transaction> sorted = new ArrayList<>();
        for (int i = 0; i < interactor.getTransactions().size(); i++) {
            Transaction tmp = interactor.getTransactions().get(i);
            if (tmp.getType().equals(TransactionType.REGULARINCOME) || tmp.getType().equals(TransactionType.REGULARPAYMENT)) {
                if (date.isAfter(tmp.getDate()) && date.isBefore(tmp.getEndDate())) {
                    sorted.add(tmp);
                    continue;
                }
            }
            if (tmp.getDate().getYear() == date.getYear()) {
                if (date.getMonth().equals(tmp.getDate().getMonth())) {
                    sorted.add(tmp);
                }
            }
        }
        this.view.setTransactions(sorted);
    }
    public void deleteTransaction(Transaction transaction) {
        if(ConnectivityBroadcastReceiver.isNetworkAvailable){
            new TransactionInteractor((TransactionInteractor.OnTransactionsGetDone)this).execute("delete",String.valueOf(transaction.getInternal_id()));
            Toast.makeText(context,"Transaction deleted",Toast.LENGTH_SHORT).show();
            refreshTransactions();
        }else{
            String url=TransactionProvider.CONTENT_URI+"/"+transaction.getId();
            Uri uri=Uri.parse(url);
            context.getContentResolver().delete(uri,null,null);
        }
    }
    public void addTransaction(Transaction newTransaction){
        if(ConnectivityBroadcastReceiver.isNetworkAvailable){
            new TransactionInteractor((TransactionInteractor.OnTransactionsGetDone)this).execute("add",String.valueOf(newTransaction.getDate()),String.valueOf(newTransaction.getTitle()),
                    String.valueOf(newTransaction.getAmount()),String.valueOf(newTransaction.getEndDate()),String.valueOf(newTransaction.getItemDescription())
                    ,String.valueOf(newTransaction.getTransactionInterval()),String.valueOf(newTransaction.getType().getType()));
            refreshTransactions();
        }else{
            ContentValues values=new ContentValues();
            values.put(DBOpenHelper.TRANSACTION_TITLE,newTransaction.getTitle());
            values.put(DBOpenHelper.TRANSACTION_ACC_ID,"1");
            values.put(DBOpenHelper.TRANSACTION_AMOUNT,newTransaction.getAmount());
            values.put(DBOpenHelper.TRANSACTION_DATE,newTransaction.getDate().toString());
            if(newTransaction.getItemDescription()!=null){
                values.put(DBOpenHelper.TRANSACTION_DESCRIPTION,newTransaction.getItemDescription());
            }else{
                values.put(DBOpenHelper.TRANSACTION_DESCRIPTION,"");
            }
            if(newTransaction.getEndDate()!=null){
                values.put(DBOpenHelper.TRANSACTION_END_DATE,newTransaction.getEndDate().toString());
            }else{
                values.put(DBOpenHelper.TRANSACTION_END_DATE,"");
            }
            if(newTransaction.getTransactionInterval()!=null){
                values.put(DBOpenHelper.TRANSACTION_INTERVAL,newTransaction.getTransactionInterval().toString());
            }else{
                values.put(DBOpenHelper.TRANSACTION_INTERVAL,"");
            }
            values.put(DBOpenHelper.TRANSACTION_INTERNAL_ID,newTransaction.getInternal_id());
            if(newTransaction.getInternal_id()==0){
                values.put(DBOpenHelper.TRANSACTION_ACTION,"add");
            }else{
                values.put(DBOpenHelper.TRANSACTION_ACTION,"update");
            }
            values.put(DBOpenHelper.TRANSACTION_TYPE,newTransaction.getType().toString());
            context.getContentResolver().insert(TransactionProvider.CONTENT_URI,values);
        }

    }
    public void updateTransaction(Transaction newTransaction,Transaction oldTransaction){
        if(ConnectivityBroadcastReceiver.isNetworkAvailable){
            new TransactionInteractor((TransactionInteractor.OnTransactionsGetDone)this).execute("update",String.valueOf(newTransaction.getDate()),String.valueOf(newTransaction.getTitle()),
                    String.valueOf(newTransaction.getAmount()),String.valueOf(newTransaction.getEndDate()),String.valueOf(newTransaction.getItemDescription())
                    ,String.valueOf(newTransaction.getTransactionInterval()),String.valueOf(newTransaction.getType().getType()),String.valueOf(oldTransaction.getInternal_id()));
            refreshTransactions();
        }else {
            Cursor c = context.getContentResolver().query(Uri.parse(TransactionProvider.CONTENT_URI+"/"+oldTransaction.getId()), null, null, null, null);
            if(c!=null&&!c.moveToFirst()){
                newTransaction.setInternal_id(oldTransaction.getInternal_id());
                addTransaction(newTransaction);

            }
            ContentValues values=new ContentValues();
            values.put(DBOpenHelper.TRANSACTION_TITLE,newTransaction.getTitle());
            values.put(DBOpenHelper.TRANSACTION_ACC_ID,"1");
            values.put(DBOpenHelper.TRANSACTION_AMOUNT,newTransaction.getAmount());
            values.put(DBOpenHelper.TRANSACTION_DATE,newTransaction.getDate().toString());
            if(newTransaction.getItemDescription()!=null){
                values.put(DBOpenHelper.TRANSACTION_DESCRIPTION,newTransaction.getItemDescription());
            }else{
                values.put(DBOpenHelper.TRANSACTION_DESCRIPTION,"");
            }
            if(newTransaction.getEndDate()!=null){
                values.put(DBOpenHelper.TRANSACTION_END_DATE,newTransaction.getEndDate().toString());
            }else{
                values.put(DBOpenHelper.TRANSACTION_END_DATE,"");
            }
            if(newTransaction.getTransactionInterval()!=null){
                values.put(DBOpenHelper.TRANSACTION_INTERVAL,newTransaction.getTransactionInterval().toString());
            }else{
                values.put(DBOpenHelper.TRANSACTION_INTERVAL,"");
            }
            values.put(DBOpenHelper.TRANSACTION_INTERNAL_ID,newTransaction.getInternal_id());
            values.put(DBOpenHelper.TRANSACTION_TYPE,newTransaction.getType().toString());
            values.put(DBOpenHelper.TRANSACTION_ACTION,"add");
            String url=TransactionProvider.CONTENT_URI+"/"+oldTransaction.getId();
            Uri uri=Uri.parse(url);
            context.getContentResolver().update(uri,values,null,null);
        }

    }
    public ArrayList<Transaction> getAllDayTransactions(){
        return interactor.getAllDayTransactions();
    }
    public ArrayList<Transaction> getAllWeekTransactions(){
        return interactor.getAllWeekTransactions();
    }
    public ArrayList<Transaction> getAllMonthTransactions(){
        return interactor.getAllMonthTransactions();
    }
    public void get(){
        if(ConnectivityBroadcastReceiver.isNetworkAvailable){
            new TransactionInteractor((TransactionInteractor.OnTransactionsGetDone)this).execute("all");
            Cursor c = context.getContentResolver().query(TransactionProvider.CONTENT_URI, null, null, null, null);
            if (c!=null&&(c.moveToFirst())) {
                ArrayList<Transaction> transactions = new ArrayList<>();
                interactor.getTransactions().clear();
                do {
                    int idPos = c.getColumnIndexOrThrow(DBOpenHelper.TRANSACTION_ID);
                    int intIdPos = c.getColumnIndexOrThrow(DBOpenHelper.TRANSACTION_INTERNAL_ID);
                    int actPos = c.getColumnIndexOrThrow(DBOpenHelper.TRANSACTION_ACTION);
                    int titlePos = c.getColumnIndexOrThrow(DBOpenHelper.TRANSACTION_TITLE);
                    int amountPos = c.getColumnIndexOrThrow(DBOpenHelper.TRANSACTION_AMOUNT);
                    int datePos = c.getColumnIndexOrThrow(DBOpenHelper.TRANSACTION_DATE);
                    int endDatePos = c.getColumnIndexOrThrow(DBOpenHelper.TRANSACTION_END_DATE);
                    int descPos = c.getColumnIndexOrThrow(DBOpenHelper.TRANSACTION_DESCRIPTION);
                    int intervalPos = c.getColumnIndexOrThrow(DBOpenHelper.TRANSACTION_INTERVAL);
                    int typePos = c.getColumnIndexOrThrow(DBOpenHelper.TRANSACTION_TYPE);
                    LocalDate endDate;
                    String desc;
                    Integer interval;
                    if(c.getString(endDatePos).equals("")){
                        endDate=null;
                    }else{
                        endDate=LocalDate.parse(c.getString(endDatePos));
                    }
                    if(c.getString(descPos).equals("")){
                        desc=null;
                    }else{
                        desc=c.getString(descPos);
                    }
                    if(c.getString(intervalPos).equals("")){
                        interval=null;
                    }else{
                        interval=c.getInt(intervalPos);
                    }
                    Transaction t = new Transaction(LocalDate.parse(c.getString(datePos)), Double.parseDouble(c.getString(amountPos)),
                            TransactionType.valueOf(c.getString(typePos)), c.getString(titlePos), desc,
                            interval, endDate, null);
                    t.setId(c.getInt(idPos));
                    t.setInternal_id(c.getInt(intIdPos));
                    if(c.getString(actPos).equals("add")){
                        addTransaction(t);
                    }else if(c.getString(actPos).equals("update")&&!t.getTitle().substring(0,7).equals("deleted")){
                        updateTransaction(t,t);
                    }else if(t.getTitle().substring(0,7).equals("deleted")){
                        deleteTransaction(t);
                    }
                } while (c.moveToNext());
                context.getContentResolver().delete(TransactionProvider.CONTENT_URI,null,null);
        }}else{
            interactor.getTransactions().clear();
            Cursor c = context.getContentResolver().query(TransactionProvider.CONTENT_URI, null, null, null, null);
            if (c!=null&&(c.moveToFirst())) {
                ArrayList<Transaction> transactions = new ArrayList<>();
                interactor.getTransactions().clear();
                do {
                    int idPos = c.getColumnIndexOrThrow(DBOpenHelper.TRANSACTION_ID);
                    int intIdPos = c.getColumnIndexOrThrow(DBOpenHelper.TRANSACTION_INTERNAL_ID);
                    int titlePos = c.getColumnIndexOrThrow(DBOpenHelper.TRANSACTION_TITLE);
                    int amountPos = c.getColumnIndexOrThrow(DBOpenHelper.TRANSACTION_AMOUNT);
                    int datePos = c.getColumnIndexOrThrow(DBOpenHelper.TRANSACTION_DATE);
                    int endDatePos = c.getColumnIndexOrThrow(DBOpenHelper.TRANSACTION_END_DATE);
                    int descPos = c.getColumnIndexOrThrow(DBOpenHelper.TRANSACTION_DESCRIPTION);
                    int intervalPos = c.getColumnIndexOrThrow(DBOpenHelper.TRANSACTION_INTERVAL);
                    int typePos = c.getColumnIndexOrThrow(DBOpenHelper.TRANSACTION_TYPE);
                    LocalDate endDate;
                    String desc;
                    Integer interval;
                    if(c.getString(endDatePos).equals("")){
                        endDate=null;
                    }else{
                        endDate=LocalDate.parse(c.getString(endDatePos));
                    }
                    if(c.getString(descPos).equals("")){
                        desc=null;
                    }else{
                        desc=c.getString(descPos);
                    }
                    if(c.getString(intervalPos).equals("")){
                        interval=null;
                    }else{
                        interval=c.getInt(intervalPos);
                    }
                    Transaction t = new Transaction(LocalDate.parse(c.getString(datePos)), Double.parseDouble(c.getString(amountPos)),
                            TransactionType.valueOf(c.getString(typePos)), c.getString(titlePos), desc,
                            interval, endDate, null);
                    t.setId(c.getInt(idPos));
                    t.setInternal_id(c.getInt(intIdPos));
                    interactor.addTransaction(t);
                    refreshTransactions();
                } while (c.moveToNext());
            }
        }

    }

    @Override
    public void onDone(ArrayList<Transaction> transactions) {
        this.view.setTransactions(transactions);
    }
}
