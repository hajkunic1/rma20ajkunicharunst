package ba.unsa.etf.rma.rma_spirala;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.ViewCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
/*
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    PRILIKOM PRELASKA IZ ONLINE U OFFLINE I OBRATNO URADITE SWIPE LIJEVO ILI DESNO. NISAM STIGAO URADITI REFRESH
    PRILIKOM OFFLINE BRISANJA U OFFLINE MODU TRANSAKCIJA SE DODAJE U LOKALNU BAZU, AKO SE PRITISNE UNDO TA SE TRANSAKCIJA BRISE LOKALNO
    SVAKA  OBRISANA TRANSAKCIJA IMA IME deleted (ime transakcije).
    PRILIKOM OSTVARENJA KONEKCIJE I USPJESNIH RADNJI ADD, UPDATE I DELETE SVI PODACI IZ TABELE TRANSAKCIJA SE BRISU
    ACCOUNT BALANS JE PRI KREIRANJU BAZE 0 I PRILIKOM USPOSTAVLJANJA KONEKCIJE NE AZURIRA SE.
    LOKALNO AZURIRANJE CE SE ODRAZITI NA SERVISU ALI NE I OBRATNO
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
 */
@RequiresApi(api = Build.VERSION_CODES.O)
public class MainActivity extends AppCompatActivity implements TransactionListFragment.OnItemClick, TransactionDetailFragment.DataTransfer,TransactionDetailFragment.Change,OnSwipe{
    private boolean twoPaneMode;
    private TransactionDetailFragment detailFragment;
    private TransactionListFragment listFragment;
    private AccountFragment accountFragment;
    private GraphsFragment graphsFragment;
    private ConnectivityBroadcastReceiver receiver = new ConnectivityBroadcastReceiver();
    private IntentFilter filter = new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE");
    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        graphsFragment=new GraphsFragment();
        listFragment=new TransactionListFragment();
        detailFragment=new TransactionDetailFragment();
        accountFragment=new AccountFragment();
        FragmentManager fragmentManager = getSupportFragmentManager();
        FrameLayout details = findViewById(R.id.transaction_detail);
        if (details != null) {
            twoPaneMode = true;
            detailFragment = (TransactionDetailFragment) fragmentManager.findFragmentById(R.id.transaction_detail);
            if (detailFragment==null) {
                detailFragment = new TransactionDetailFragment();
                fragmentManager.beginTransaction().
                        replace(R.id.transaction_detail,detailFragment)
                        .commit();
            }
        } else {
            twoPaneMode = false;
        }
        Fragment fragment =fragmentManager.findFragmentById(R.id.transaction_list);
        if (fragment==null){
            fragment = new TransactionListFragment();
            fragmentManager.beginTransaction()
                    .replace(R.id.transaction_list,fragment)
                    .commit();
        }
        else{
            fragmentManager.beginTransaction()
                    .replace(R.id.transaction_list,listFragment)
                    .commit();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onItemClicked(Transaction t,String sortBy,String filterBy,Account acc,String date) {
        Bundle arguments = new Bundle();
        arguments.putParcelable("transaction", t);
        arguments.putParcelable("acc",acc);
        arguments.putString("date",date);
        arguments.putString("sortBy",sortBy);
        arguments.putString("filterBy",filterBy);
        detailFragment = new TransactionDetailFragment();
        detailFragment.setArguments(arguments);
        if (twoPaneMode){
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.transaction_detail, detailFragment)
                    .commit();
        }
        else{
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.transaction_list,detailFragment)
                    .addToBackStack(null)
                    .commit();
        }
    }

    @Override
    public void onSwipe(Account account) {
        Bundle arg=new Bundle();
        arg.putParcelable("acc",account);
        accountFragment=new AccountFragment();
        accountFragment.setArguments(arg);
        if(!twoPaneMode){
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.transaction_list, accountFragment)
                    .commit();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        registerReceiver(receiver,filter);
    }

    @Override
    public void onPause() {
        super.onPause();
        unregisterReceiver(receiver);
    }


    @Override
    public void passData(Transaction t, Transaction t1, String action) {
        Bundle arg=new Bundle();
        arg.putParcelable("transaction",t);
        arg.putString("type",action);
        if(t1!=null)
            arg.putParcelable("oldTransaction",t1);
        TransactionListFragment transactionListFragment=new TransactionListFragment();
        transactionListFragment.setArguments(arg);
        getSupportFragmentManager().beginTransaction().add(transactionListFragment,"transaction").commit();
        getSupportFragmentManager().beginTransaction().remove(transactionListFragment).commit();
        if (twoPaneMode){
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.transaction_detail, new TransactionDetailFragment())
                    .commit();
        }
        else{
            getSupportFragmentManager().popBackStack(null,FragmentManager.POP_BACK_STACK_INCLUSIVE);

        }
    }

    @Override
    public void onChange(final String sortBy, final String filterBy, final String date) {
        Handler handler = new Handler();
        if(twoPaneMode)
            handler.postDelayed(new Runnable()
            {
                @Override
                public void run()
                {
                    listFragment.getPresenter().listSort(sortBy,filterBy,LocalDate.parse(date));
                }
            }, 100);
    }

    @Override
    public void onSwipe(String direction, String currentFragment) {
        if(!twoPaneMode){
            if(direction.equals("right")){
                if(currentFragment.equals("account")){
                    getSupportFragmentManager().beginTransaction()
                            .replace(R.id.transaction_list, listFragment)
                            .commit();
                }else if(currentFragment.equals("list")){
                    Bundle arg=new Bundle();
                    arg.putParcelableArrayList("month",listFragment.getPresenter().getAllMonthTransactions());
                    arg.putParcelableArrayList("week",listFragment.getPresenter().getAllWeekTransactions());
                    arg.putParcelableArrayList("day",listFragment.getPresenter().getAllDayTransactions());
                    graphsFragment.setArguments(arg);
                    getSupportFragmentManager().beginTransaction()
                            .replace(R.id.transaction_list, graphsFragment)
                            .commit();
                }else if(currentFragment.equals("graphs")){
                    getSupportFragmentManager().beginTransaction()
                            .replace(R.id.transaction_list, accountFragment)
                            .commit();
                }
            }else if(direction.equals("left")){
                if(currentFragment.equals("account")){
                    Bundle arg=new Bundle();
                    arg.putParcelableArrayList("month",listFragment.getPresenter().getAllMonthTransactions());
                    arg.putParcelableArrayList("week",listFragment.getPresenter().getAllWeekTransactions());
                    arg.putParcelableArrayList("day",listFragment.getPresenter().getAllDayTransactions());
                    graphsFragment.setArguments(arg);
                    getSupportFragmentManager().beginTransaction()
                            .replace(R.id.transaction_list, graphsFragment)
                            .commit();
                }else if(currentFragment.equals("graphs")){
                    getSupportFragmentManager().beginTransaction()
                            .replace(R.id.transaction_list, listFragment)
                            .commit();
                }
            }
        }

    }
}
