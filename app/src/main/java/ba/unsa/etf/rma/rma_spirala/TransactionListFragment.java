package ba.unsa.etf.rma.rma_spirala;

/**

Prilikom swipe sa glavnog ekrana na account ekran sacekati sekundu da se transakcije ucitaju, inace ce se app krahirati jer je account null
 **/

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Parcelable;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.core.content.ContextCompat;
import androidx.core.view.ViewCompat;
import androidx.fragment.app.Fragment;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Objects;

import javax.crypto.CipherOutputStream;

public class TransactionListFragment extends Fragment implements ITransactionView{
    private Spinner sortBySpinner,filterBySpinner;
    private ListView transactionListView;
    private TransactionPresenter transactionPresenter;
    private TransactionAdapter transactionAdapter;
    private FilterBySpinnerAdapter filterByAdapter;
    private Button btnLeft,btnRight,addTransaction;
    private TextView month, limit, globalAmount;
    public LocalDate date=LocalDate.now();
    private OnItemClick onItemClick;
    private View itemPosition;
    private Context context;
    private AccountPresenter accountPresenter;
    private OnSwipe onSwipe;

    public TransactionPresenter getPresenter() {
        if (transactionPresenter == null) {
            transactionPresenter = new TransactionPresenter(this, getActivity());
        }
        return transactionPresenter;
    }
    public AccountPresenter getAccPresenter() {
        if (accountPresenter == null) {
            accountPresenter = new AccountPresenter(getActivity());
        }
        accountPresenter.get();
        return accountPresenter;
    }

    @Override
    public void setTransactions(ArrayList<Transaction> transactions) {
        if(transactionAdapter!=null)
            transactionAdapter.setTransactions(transactions);
    }

    @Override
    public void notifyTransactionListDataSetChanged() {
        transactionAdapter.notifyDataSetChanged();
    }

    public interface OnItemClick {
        public void onItemClicked(Transaction t, String sortBy, String filterBy,Account acc,String date);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }
    @Override
    public void onAttach(Context context){
        super.onAttach(context);
        this.context=context;
    }
    @SuppressLint("ClickableViewAccessibility")
    @Override
    public View onCreateView(
            LayoutInflater inflater,
            ViewGroup container,
            Bundle savedInstanceState) {

        Toast toast = Toast.makeText(getContext(), "Prilikom prelaska iz online u offline i obratno uradite swipe lijevo ili desno.Nistam stigao napraviti refresh", Toast.LENGTH_LONG);
        toast.show();
        View fragmentView = inflater.inflate(R.layout.fragment_list, container, false);
        ViewCompat.setNestedScrollingEnabled(fragmentView.findViewById(R.id.transaction_list_view),true);
        sortBySpinner=fragmentView.findViewById(R.id.sort_by_spinner);
        ArrayAdapter<CharSequence> sortByAdapter=ArrayAdapter.createFromResource(context,R.array.sort_by_items,android.R.layout.simple_spinner_item);
        sortByAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sortBySpinner.setAdapter(sortByAdapter);
        transactionListView=fragmentView.findViewById(R.id.transaction_list_view);
        transactionAdapter=new TransactionAdapter(context,R.layout.list_element,new ArrayList<Transaction>());
        transactionListView.setAdapter(transactionAdapter);
        getPresenter();
        transactionPresenter.get();
        if(!ConnectivityBroadcastReceiver.isNetworkAvailable){
            transactionPresenter.filterByMonth(date);
        }
        filterBySpinner=fragmentView.findViewById(R.id.filter_by_spinner);
        ArrayList<String> types=new ArrayList<String>(){
            {
                add("Filter by");
                add("ALL");
                add("INDIVIDUALPAYMENT");
                add("REGULARPAYMENT");
                add("PURCHASE");
                add("INDIVIDUALINCOME");
                add("REGULARINCOME");
            }
        };
        filterByAdapter=new FilterBySpinnerAdapter(context,R.layout.filter_by_spinner_element,types);
        filterBySpinner.setAdapter(filterByAdapter);
        filterBySpinner.setSelection(1);
        limit=fragmentView.findViewById(R.id.limit_label);
        globalAmount=fragmentView.findViewById(R.id.global_amount_label);
        Handler handler = new Handler();
        getAccPresenter();
        handler.postDelayed(new Runnable()
        {
            @Override
            public void run()
            {
                limit.setText(String.format("Limit: %s", accountPresenter.getAcc().getTotalLimit()));
                globalAmount.setText(String.format("Global amount: %s", accountPresenter.getAcc().getBudget()));
            }
        }, 3000);
        month=fragmentView.findViewById(R.id.month_label);
        month.setText(date.getMonth()+","+date.getYear());
        addTransaction=fragmentView.findViewById(R.id.add_transaction_btn);
        transactionListView.setOnItemClickListener(listItemClickListener);
        onItemClick= (OnItemClick) getActivity();
        onSwipe=(OnSwipe)getActivity();
        if(getArguments()!=null){
            if(getArguments().getString("type").equals("delete")){
                transactionPresenter.deleteTransaction((Transaction)getArguments().getParcelable("transaction"));
            }else if(getArguments().getString("type").equals("update")){
                transactionPresenter.updateTransaction((Transaction)getArguments().getParcelable("transaction"),(Transaction)getArguments().getParcelable("oldTransaction"));
            }else if(getArguments().getString("type").equals("add")){
                transactionPresenter.addTransaction((Transaction)getArguments().getParcelable("transaction"));
            }
        }
        addTransaction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String result = filterByAdapter.getType(filterBySpinner.getSelectedItemPosition());
                TextView textView1 = (TextView)sortBySpinner.getSelectedView();
                String result1=textView1.getText().toString();
                onItemClick.onItemClicked(null,result1,result,accountPresenter.getAcc(),date.toString());
            }

        });

        sortBySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener(){

            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String selectedItemText = (String) parent.getItemAtPosition(position);
                String result = filterByAdapter.getType(filterBySpinner.getSelectedItemPosition());
                transactionPresenter.listSort(selectedItemText,result,date);
                addTransaction.setEnabled(true);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }


        });

        filterBySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener(){

            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String selectedItemText = (String) parent.getItemAtPosition(position);
                TextView textView = (TextView)sortBySpinner.getSelectedView();
                String result;
                if(textView!=null) {
                    result = textView.getText().toString();
                    transactionPresenter.listSort(result, selectedItemText, date);
                }
                addTransaction.setEnabled(true);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }


        });
        btnLeft=fragmentView.findViewById(R.id.prev_month);
        btnRight=fragmentView.findViewById(R.id.next_month);
        btnLeft.setOnClickListener(new View.OnClickListener(){
            @SuppressLint("SetTextI18n")
            public void onClick(View v) {
                date=date.minusMonths(1);
                month.setText(date.getMonth()+","+date.getYear());
                String result = filterByAdapter.getType(filterBySpinner.getSelectedItemPosition());
                TextView textView1 = (TextView)sortBySpinner.getSelectedView();
                String result1;
                if(textView1!=null) {
                    result1 = textView1.getText().toString();
                    transactionPresenter.listSort(result1, result, date);
                }
                if(!ConnectivityBroadcastReceiver.isNetworkAvailable){
                    transactionPresenter.filterByMonth(date);
                }
                addTransaction.setEnabled(true);
            }
        });
        btnRight.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("SetTextI18n")
            public void onClick(View v) {
                date=date.plusMonths(1);
                month.setText(date.getMonth()+","+date.getYear());
                String result = filterByAdapter.getType(filterBySpinner.getSelectedItemPosition());
                TextView textView1 = (TextView)sortBySpinner.getSelectedView();
                String result1;
                if(textView1!=null) {
                    result1 = textView1.getText().toString();
                    transactionPresenter.listSort(result1, result, date);
                }
                if(!ConnectivityBroadcastReceiver.isNetworkAvailable){
                    transactionPresenter.filterByMonth(date);
                }
                addTransaction.setEnabled(true);
            }
        });


        fragmentView.setOnTouchListener(new OnSwipeTouchListener(getActivity()){
            @Override
                public void onSwipeLeft(){
                    onSwipe.onSwipe(getAccPresenter().getAcc());
                }
            @Override
                public void onSwipeRight(){
                onSwipe.onSwipe("right","list");
            }
        });

        transactionListView.setOnTouchListener(new OnSwipeTouchListener(getActivity()){
            @Override
            public void onSwipeLeft(){
                onSwipe.onSwipe(getAccPresenter().getAcc());
            }
            @Override
            public void onSwipeRight(){
                onSwipe.onSwipe("right","list");
            }

        });

        return fragmentView;
    }

    private AdapterView.OnItemClickListener listItemClickListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            Transaction transaction = transactionAdapter.getTransaction(position);
            ColorDrawable grey=new ColorDrawable();
            grey.setColor(getResources().getColor(android.R.color.darker_gray));
            ColorDrawable white=new ColorDrawable();
            white.setColor(getResources().getColor(android.R.color.white));
            if(view!=itemPosition&&itemPosition!=null){
                itemPosition.setBackground(white);
                addTransaction.setEnabled(true);
            }
            if(((ColorDrawable)view.getBackground()).getColor()==white.getColor()){
                view.setBackground(grey);
                addTransaction.setEnabled(false);
                onItemClick.onItemClicked(transaction,sortBySpinner.getSelectedItem().toString(),filterByAdapter.getType(filterBySpinner.getSelectedItemPosition()),accountPresenter.getAcc(),date.toString());
            }else{
                addTransaction.setEnabled(true);
                view.setBackground(white);
                onItemClick.onItemClicked(null,sortBySpinner.getSelectedItem().toString(),filterByAdapter.getType(filterBySpinner.getSelectedItemPosition()),accountPresenter.getAcc(),date.toString());
            }
            itemPosition=view;


        }
    };

     @Override
        public void onResume() {
         super.onResume();

     }
}
