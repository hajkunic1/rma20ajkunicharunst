package ba.unsa.etf.rma.rma_spirala;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.net.PortUnreachableException;

public class DBOpenHelper extends SQLiteOpenHelper {

    public static final String DATABASE_NAME = "hajkunic1.db";
    public static final int DATABASE_VERSION = 1;
    public static final String TRANSACTION_TABLE = "transactions";
    public static final String TRANSACTION_ID = "id";
    public static final String TRANSACTION_INTERNAL_ID="in_id";
    public static final String TRANSACTION_TITLE = "title";
    public static final String TRANSACTION_AMOUNT = "amount";
    public static final String TRANSACTION_DATE = "date";
    public static final String TRANSACTION_END_DATE = "endDate";
    public static final String TRANSACTION_TYPE = "type";
    public static final String TRANSACTION_ACC_ID = "accId";
    public static final String TRANSACTION_DESCRIPTION = "description";
    public static final String TRANSACTION_INTERVAL = "interval";
    public static final String TRANSACTION_ACTION = "act";
    public static final String ACCOUNT_TABLE = "account";
    public static final String ACCOUNT_ID = "id";
    public static final String BUDGET = "budget";
    public static final String TOTAL_LIMIT = "totalLimit";
    public static final String MONTH_LIMIT = "monthLimit";
    public static final String ACTION = "act";
    private static final String ACCOUNT_TABLE_CREATE =
            "CREATE TABLE IF NOT EXISTS " + ACCOUNT_TABLE + " (" + ACCOUNT_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                    + BUDGET + " TEXT NOT NULL, "
                    + TOTAL_LIMIT + " TEXT NOT NULL,"
                    + ACTION + " TEXT, "
                    + MONTH_LIMIT + " TEXT NOT NULL);";
    private static final String ACCOUNT_TABLE_DROP = "DROP TABLE IF EXISTS " + ACCOUNT_TABLE;
    private static final String TRANSACTION_TABLE_CREATE =
            "CREATE TABLE IF NOT EXISTS " + TRANSACTION_TABLE + " ("  + TRANSACTION_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                    + TRANSACTION_TITLE + " TEXT NOT NULL, "
                    + TRANSACTION_INTERNAL_ID + " TEXT NOT NULL, "
                    + TRANSACTION_ACTION + " TEXT, "
                    + TRANSACTION_AMOUNT + " TEXT, "
                    + TRANSACTION_DATE + " TEXT, "
                    + TRANSACTION_END_DATE + " TEXT, "
                    + TRANSACTION_TYPE + " TEXT, "
                    + TRANSACTION_ACC_ID + " TEXT NOT NULL, "
                    + TRANSACTION_DESCRIPTION + " TEXT, "
                    + TRANSACTION_INTERVAL + " TEXT, "+" FOREIGN KEY ("+TRANSACTION_ACC_ID+") REFERENCES "+ ACCOUNT_TABLE +"("+ACCOUNT_ID+"));";
    private static final String TRANSACTION_TABLE_DROP = "DROP TABLE IF EXISTS " + TRANSACTION_TABLE;
    private String itemDescription;
    private Integer transactionInterval;

    public DBOpenHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    public DBOpenHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(ACCOUNT_TABLE_CREATE);
        db.execSQL(TRANSACTION_TABLE_CREATE);
        db.execSQL("INSERT INTO "+ ACCOUNT_TABLE +" VALUES (1,0,0,'init',0);");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(TRANSACTION_TABLE_DROP);
        db.execSQL(ACCOUNT_TABLE_DROP);
        onCreate(db);
    }
}
