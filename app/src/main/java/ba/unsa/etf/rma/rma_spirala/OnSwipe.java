package ba.unsa.etf.rma.rma_spirala;

public interface OnSwipe {

    void onSwipe(String direction, String currentFragment);
    void onSwipe(Account account);
}
