package ba.unsa.etf.rma.rma_spirala;

import java.util.ArrayList;

public interface ITransactionInteractor {
    ArrayList<Transaction> getTransactions();
}
